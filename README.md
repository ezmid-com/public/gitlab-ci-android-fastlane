# gitlab-ci-android-fastlane
This Docker image contains the Android SDK and most common packages necessary for building Android apps in a CI tool like GitLab CI (Android SDK, git, fastlane). Make sure your CI environment's caching works as expected, this greatly improves the build time, especially if you use multiple build jobs.

```
docker run -it --rm registry.gitlab.com/ezmid-com/public/gitlab-ci-android-fastlane
```

A `.gitlab-ci.yml` with caching of your project's dependencies would look like this:

```
image: registry.gitlab.com/ezmid-com/public/gitlab-ci-android-fastlane

stages:
- build

before_script:
- export GRADLE_USER_HOME=$(pwd)/.gradle
- chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
  - .gradle/

build:
  stage: build
  script:
  - ./gradlew build
  artifacts:
    paths:
    - app/build/outputs
```

## Development

To build image

```
docker build -t registry.gitlab.com/ezmid-com/public/gitlab-ci-android-fastlane:TAG .
```

To deploy image

```
docker push -t registry.gitlab.com/ezmid-com/public/gitlab-ci-android-fastlane:TAG
```