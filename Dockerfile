FROM phusion/baseimage:jammy-1.0.1
LABEL maintainer="Ezmid.com <ezmidapps@gmail.com>"

CMD ["/sbin/my_init"]

ENV LC_ALL "en_US.UTF-8"
ENV LANGUAGE "en_US.UTF-8"
ENV LANG "en_US.UTF-8"

ENV VERSION_BUILD_TOOLS "33.0.2"
ENV VERSION_TARGET_SDK "33"

ENV ANDROID_SDK_ROOT "/sdk"

ENV PATH "$PATH:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin"
ENV DEBIAN_FRONTEND noninteractive

ENV HOME "/root"

RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update
RUN apt-get -y install --no-install-recommends \
    curl \
    openjdk-8-jdk \
    openjdk-17-jdk \
    unzip \
    zip \
    git \
    build-essential \
    file \
    ssh \
    zlib1g-dev \
    libreadline-dev

# Install rbenv
ENV RBENV_ROOT "/rbenv"
RUN git clone https://github.com/rbenv/rbenv.git ${RBENV_ROOT}
RUN git clone https://github.com/rbenv/ruby-build.git ${RBENV_ROOT}/plugins/ruby-build
RUN ${RBENV_ROOT}/plugins/ruby-build/install.sh
ENV PATH ${RBENV_ROOT}/shims:${RBENV_ROOT}/bin:$PATH
RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh


# Install ruby-build plugin


# Install Ruby 2.6
RUN rbenv install 2.6.10 \
  && rbenv global 2.6.10 \
  && rbenv rehash

ADD https://dl.google.com/android/repository/commandlinetools-linux-8512546_latest.zip /tools.zip
RUN unzip /tools.zip && rm -rf /tools.zip
RUN mv /cmdline-tools /tools
RUN mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools
RUN mv /tools ${ANDROID_SDK_ROOT}/cmdline-tools/

RUN yes | sdkmanager --licenses

RUN mkdir -p $HOME/.android && touch $HOME/.android/repositories.cfg
RUN sdkmanager "platform-tools" "tools" "platforms;android-${VERSION_TARGET_SDK}" "build-tools;${VERSION_BUILD_TOOLS}"
RUN sdkmanager "extras;android;m2repository" "extras;google;google_play_services" "extras;google;m2repository"

RUN apt-get -y remove openjdk-8-jdk


RUN gem install fastlane bundler

ENV FIREBASE_PATH "/usr/local/bin/firebase"
ADD https://firebase.tools/bin/linux/latest $FIREBASE_PATH
RUN chmod +x $FIREBASE_PATH

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*